FROM alpine:latest

LABEL maintainer="Autotoolr <https://gitlab.com/autotoolr-docker>" cli_version="2.33.1"
LABEL org.autotoolr.meta.included_package.1="azure-cli"
LABEL org.autotoolr.meta.included_package.2="terraform"

RUN apk --update add python3 bash &&     if [ ! -e /usr/bin/python ]; then ln -sf python3 /usr/bin/python ; fi &&     apk add --virtual=build gcc libffi-dev musl-dev openssl-dev python3-dev make &&     python3 -m ensurepip &&     rm -r /usr/lib/python*/ensurepip &&     pip3 install --no-cache --upgrade pip setuptools wheel &&     if [ ! -e /usr/bin/pip ]; then ln -s pip3 /usr/bin/pip ; fi &&     pip3 install --upgrade azure-cli==2.23.0 &&     apk --purge del build &&     rm /var/cache/apk/*
RUN apk -v --update add ca-certificates &&     apk add --virtual=build curl tar zip gzip &&     curl -LO https://releases.hashicorp.com/terraform/1.1.6/terraform_1.1.6_linux_amd64.zip &&     unzip terraform_1.1.6_linux_amd64.zip &&     chmod +x ./terraform &&     mv ./terraform /usr/local/bin/terraform &&     apk --purge del build &&     rm /var/cache/apk/*
